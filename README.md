# Pointe au Sel

La théorie du noyau central, une méthode de représentation des données sociales, s’appuie sur le tri de mots exprimés et ordonnés lors d’entrevues individuelles. Ce travail fastidieux de tri a déjà été automatisé par le passé mais la solution n’est plus disponible. Vous trouverez ici une nouvelle solution logicielle permettant au chercheur de réaliser une analyse prototypique et une analyse catégorielle  à partir d’un exercice d’association libre de mots, en obtenant le classement en quatre ensembles, le noyau et ses périphéries. 

Mots-clés : association libre de mots ;analyse prototypique, analyse catégorielle ; représentations sociales, 

Central core theory, a social representation method, sort ranked words mentioned during individual interviews. This boring task has been computerized in the past, but the software is not downloadable anymore. Find here a new software which allows the researcher to perform prototypical and categorical analyses from free association tasks, and obtain four sets of words, the core and his peripheries.

Keywords : free tasks ; prototypical rank-frequency analysis, categorical analysis; social representations

_____________________________

Pointe-au-Sel a été développé avec Microsoft Access  (MicroSoft Office 365)

_____________________________

Installation:
- installer Microsoft Access
- télécharger le fichier exécutable   pointeausel-x-x.accdr  ( 'x-x' étant le numéro de version )

Un jeu d'essai ( test_Pointe-au-sel.csv ) est proposé pour illustrer la structure du fichier attendu en entrée et parcourir les différents menus dans l'ordre fonctionnel. 

_____________________________

Rapport interne: "Description d'une solution logicielle, nommée Pointe au Sel, pour réaliser des analyses prototypiques"
hal.univ-reunion.fr/hal-03468878v1 (DOI 10.26171/ekb2-jf83)

Téléchargement de Pointe-au-Sel (DOI 10.26171/pointe-au-sel) :  framagit.org/rouet/pointe-au-sel

_____________________________

-  UMR Espace-dev (https://www.espace-dev.fr)
-  Université de La Réunion (https://www.univ-reunion.fr/)
-  philippe.rouet@univ-reunion.fr


